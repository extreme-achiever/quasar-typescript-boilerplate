const getConfig = () => ({
  API_URL: process.env.API_URL
});

console.log(getConfig());

export default getConfig;
