import { Post } from '../../../sdk/domain/Post';

export interface AppState {
  posts: Post[];
  bookmarkedPosts: Post[];
  selectedPost: Post | null;
}

export type SetBookmarkListHandler = (state: AppState, data: Post[]) => void;
export type SetPostListHandler = (state: AppState, data: Post[]) => void;
export type SetSelectedPostHandler = (state: AppState, data: Post) => void;
