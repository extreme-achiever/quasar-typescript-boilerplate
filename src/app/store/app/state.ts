const getInitialState = () => ({
  posts: [],
  bookmarkedPosts: [],
  selectedPost: null
});

const initialState = getInitialState();

export default initialState;
