import {
  SetBookmarkListHandler,
  SetPostListHandler,
  SetSelectedPostHandler
} from './types';

export const setBookmarkList: SetBookmarkListHandler = (state, data) => {
  state.posts = data;
};

export const setPostList: SetPostListHandler = (state, data) => {
  state.bookmarkedPosts = data;
};

export const setSelectedPost: SetSelectedPostHandler = (state, data) => {
  state.selectedPost = data;
};
