import Vue from 'vue';
import Vuex from 'vuex';
import app from './app';
Vue.use(Vuex);
export const Store = new Vuex.Store({
  modules: {
    app
  },
  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV === 'true'
});
export default function(/* { ssrContext } */) {
  return Store;
}
//# sourceMappingURL=index.js.map
